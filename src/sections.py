"""Contains classes defining sections of text.
"""


class Section:
    """Section of content (such as a chapter, subsection, example, etc.)

    `Section`s are primarily composed of a title, a text body (if given), and
    other sections that it contains (considered subsections of this section).
    """
    def __init__(self, title,
            text=open('content/BLANK.txt'), subsections=None):
        """Construct Section.

        Args:
            title (str): Title of this section.
            text (file):
                Text contents of this section. Usually a *.txt file. Default is
                a blank file.
            subsections (List[Section]):
                List of :class:`Section` objects contained by this object.
                For example, subsections of a section should go here. The order
                of the sections is the order they will be rendered.
        """
        self._title = title
        self._text = text
        self._subsections = subsections
        if not self._subsections:
            self._subsections = []

    def __del__(self):
        """Destruct self."""
        self._text.close()

    def __str__(self):
        """String overload."""
        rep_str = "Section: " + self._title + "\n\tContains Sections:\n"
        for section in self._subsections:
            rep_str += '\t\t - '
            rep_str += section.title

        return rep_str

    __repr__ = __str__  # TODO make a better repr

    @property
    def title(self):
        """str: Title of section."""
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    def display(self):
        """Print this section, its subsections, and their contents to terminal.
        """
        print(self._title, '\n')
        print(self._text.read())

        for section in self._subsections:
            section.display()


class Heading(Section):
    """
    Hierarchical subdivisions (such as chapters, sections, subsections).
    """
    def __init__(self, *args, numbered=True, **kwargs):
        """Construct Heading.

        Args:
            *args (tuple): Required args from superclass.
            numbered (bool):
                If True, the heading will be numbered, in an outline format.
                If False, the heading will not contribute to the outline
                numbering.
            **kwargs (tuple): Keyword args from superclass.
        """
        super().__init__(*args, **kwargs)

        self._numbered = numbered


# TODO I'm not sure if I should keep making everything sections, or if I should
# TODO make a separate 'Definition' class and a 'Paragraph' class... DONO
class Definition(Section):
    """
    Definition of some terminology (like a word in a dictionary).
    """
    def __init__(self, *args, **kwargs):
        """Construct Definition.

        Args:
            *args (tuple): Required args from superclass.
            **kwargs (tuple): Keyword args from superclass.
        """
        super().__init__(*args, **kwargs)
