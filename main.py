from src import sections as sec


def main():
    preface = sec.Heading("Preface",
                          text=open('content/preface.txt'), numbered=False)
    intro   = sec.Heading("Introduction",
                          text=open('content/intro.txt'), numbered=False)

    book = sec.Section("Learning SQL", subsections=[preface, intro])

    book.display()


if __name__ == '__main__':
    main()
