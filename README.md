Welcome to my SQL learning project!

The main goal of this project is to help me keep track of what I learn in SQL.
It contains a python library for making generalized interactive documents, and
uses those to keep my SQL notes.